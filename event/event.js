    document.querySelector(".add").addEventListener('click', function(){document.querySelector('#addModal').style.display='block';});
    let deleteButton = document.querySelectorAll('.delete');
    let editModal = document.querySelector("#editModal");
    for(var temp of deleteButton){
        temp.addEventListener('click', function(event){
            document.getElementById('confirmModal').style.display='block';
            document.querySelector(".deletebtn").onclick = function(){
                addNotificationObject(event.target.closest("tr").childNodes[5].innerHTML + " deleted");
                event.target.closest("tr").remove();
                document.querySelector('#confirmModal').style.display = 'none';
            }
        });
        temp.parentElement.children[0].addEventListener('click', function(event){
            editModal.style.display='block';
            editModal.querySelector("#accept").onclick = function(){
                event.target.closest("tr").childNodes[5].innerHTML = editModal.querySelector("#nameinput").value;
                event.target.closest("tr").childNodes[7].innerHTML = editModal.querySelector("#groupinput").value;
                event.target.closest("tr").childNodes[9].innerHTML = editModal.querySelector("#genderinput").value;
                event.target.closest("tr").childNodes[11].innerHTML = editModal.querySelector("#birthdayinput").value;
                addNotificationObject(event.target.closest("tr").childNodes[5].innerHTML + " edited");
                editModal.style.display = 'none';
                editModal.reset();
            }
        });
    }
    document.querySelector("#cancelAdd").addEventListener('click', function(event){
        event.preventDefault();
        document.querySelector('#addModal').style.display = 'none';
    });
    document.querySelector('#accept').addEventListener('click', function(event){
        event.preventDefault(); 
        addStudentEvent();
    });
    document.querySelector(".cancelbtn").addEventListener('click', function(){
        document.querySelector('#confirmModal').style.display = 'none';
    });
    if(!document.querySelector('.bellpopupcl').childNodes){
        document.querySelector('.bellpopupcl').style.display = 'none';
    }
    UpdateCache();
function addStudentEvent(){
    const mainTableOb = document.querySelector("tbody");
    const addRow = document.createElement('tr');
    mainTableOb.appendChild(addRow);

    let addCell = document.createElement('td');
    const checkboxElement = document.createElement('input');
    checkboxElement.type = 'checkbox';
    checkboxElement.classList.add('checkbox');
    addCell.appendChild(checkboxElement);
    addRow.appendChild(addCell);

    addCell = document.createElement('td');
    let formText = document.querySelector('#nameinput').value;
    let newOption = document.createElement('option');
    newOption.value = formText;
    document.querySelector("#name-list").appendChild(newOption);
    document.querySelector('#nameinput').value = "";
    let temp = false;
    for(let i = 0; i < localStorage.length; i++){
        if(localStorage.getItem(i) == formText){
            temp = true;
            break;
        }
    }
    if(!temp){
        localStorage.setItem(localStorage.length, formText);
    }
    let p = document.createElement('p');
    p.innerHTML = formText;
    addCell.appendChild(p);
    addRow.appendChild(addCell);
    addNotificationObject(formText + " added");

    addCell = document.createElement('td');
    formText = document.querySelector('#groupinput').value;
    document.querySelector('#groupinput').value = "";
    p = document.createElement('p');
    p.innerHTML = formText;
    addCell.appendChild(p);
    addRow.appendChild(addCell);

    addCell = document.createElement('td');
    formText = document.querySelector('#genderinput').value;
    document.querySelector('#genderinput').value = "";
    p = document.createElement('p');
    p.innerHTML = formText;
    addCell.appendChild(p);
    addRow.appendChild(addCell);

    addCell = document.createElement('td');
    formText = document.querySelector('#birthdayinput').value;
    document.querySelector('#birthdayinput').value = "";
    p = document.createElement('p');
    p.innerHTML = formText;
    addCell.appendChild(p);
    addRow.appendChild(addCell);

    addCell = document.createElement('td');
    var statusElement = document.createElement('div');
    statusElement.classList.add('statusCircle');
    addCell.appendChild(statusElement);
    addRow.appendChild(addCell);

    addCell = document.createElement('td');
    var editElement = document.createElement('button');
    var deleteElement = document.createElement('button');
    editElement.classList.add('edit');
    deleteElement.classList.add('delete');
    let editButton = document.createElement('img');
    editButton.src = './img/iconmonstr-pencil-14.svg';
    let deleteButton = document.createElement('img');
    deleteButton.src = './img/cross-svgrepo-com.svg';
    editElement.appendChild(editButton);
    deleteElement.appendChild(deleteButton);
    deleteElement.addEventListener('click', function(event){
        confirm(event);});
    addCell.appendChild(editElement);
    addCell.appendChild(deleteElement);
    
    addRow.appendChild(addCell);
    document.querySelector('#addModal').style.display = 'none';

}
function bellAnimationFunction()
{
    document.getElementById("bellId").classList.remove("bellAnimationClass");
    document.querySelector(".belldot").style.display = 'none';
}
function checkAll()
{
    var ca = document.getElementById("checkboxAll").checked;
    var checkBoxs = document.getElementsByClassName("checkbox");
    for(var c of checkBoxs)
    {
        c.checked = ca;
    }
}
function addNotificationObject(text){
    let popup = document.querySelector('.bellpopupcl');
    let notificationBlock = document.createElement('div');
    popup.appendChild(notificationBlock);
    notificationBlock.classList.add('addedNotification');
    let notificationText = document.createElement('p');
    notificationBlock.appendChild(notificationText);
    notificationText.classList.add('p');
    notificationText.innerHTML = text;
    document.querySelector(".belldot").style.display = 'block';
    document.getElementById("bellId").classList.add("bellAnimationClass");
}
function UpdateCache(){
    let list = document.querySelector('#name-list');
    if(list.childNodes.length == 0){
        for(let i = 0; i < localStorage.length; i++){
            let newOption = document.createElement('option');
            newOption.value = localStorage.getItem(i);
            list.appendChild(newOption);
        }
    }
}